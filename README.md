# nagios-plugin-mongodb

Nagios plugin to check MongoDB.

These instructions are for Debian 10 and Fedora 28.  Minor alterations may be needed for other systems.

## Usage

```bash
check_mongodb.sh -h hostname -u username -x command [-w warning] [-c critical]
```

The script will lookup the specified hostname-username combination in a database credentials file which is in the same format as used by Postgres (hostname:port:database:username:password).  This file must be located at ~nagios/.mongopass, where ~nagios is the home directory of the user that owns the Nagios server process.  On Debian 10, this is /var/lib/nagios/.  On Fedora 28, this is /var/spool/nagios/.  If the user owning the Nagios server process is `nagios`, then you can find the home directory with the command `echo ~nagios`.

`command` is one of the following.

* primary     - Check that hostname is the primary (-w and -c are not applicable).
* secondary   - Check that hostname is a secondary (-w and -c are not applicable).
* replication - Check replication status of hostname (-w and -c are not applicable).

## Installation

Create a MongoDB user for monitoring on each server to be monitored.  For replica sets, do this only on the primary.  This assumes that admin is the authentication database.  The roles may vary for custom monitoring.

```
use admin
db.createUser(
    {
        user: "monitor",
        pwd: "PASSWORD",
        customData: { name: "Monitor user" },
        roles: [{ db: "admin", role: "clusterMonitor" }, { db: "monitor", role: "readWrite" }]
    }
)
```

The remaining steps are performed on the Nagios server.

Install jq (https://stedolan.github.io/jq/) and the MongoDB CLI client.  On Debian this is done with the command `apt install jq mongodb`.  On Fedora, `dnf install jq mongodb`.

Copy this plugin to the Nagios plugins directory.  On Debian, this is done as follows.  On Fedora, the commands are the same except replace `/usr/lib` with `/usr/lib64`.

```bash
cp -i check_mongodb.sh /usr/lib/nagios/plugins/
chown root:root /usr/lib/nagios/plugins/check_mongodb.sh
chmod 0755 /usr/lib/nagios/plugins/check_mongodb.sh
# If running SELinux, then also run this.
restorecon -F /usr/lib/nagios/plugins/check_mongodb.sh
```

Create a MongoDB credentials file at ~nagios/.mongopass (assuming nagios is the Nagios process username).

```bash
# Login as root.
touch ~nagios/.mongopass
chown nagios:nagios ~nagios/.mongopass
chmod 0600 ~nagios/.mongopass
```

The credentials file must include one line in the following format for each monitored system.  Passwords may contain colons.

```
hostname:port:database:username:password
```

Add the following custom command to the Nagios server commands configuration /etc/nagios4/objects/commands.cfg on Debian.  Or preferrablly create a new commands-custom.cfg for custom commands, then include this in the main Nagios configuration file /etc/nagios4/nagios.cfg.  On Fedora, the directory is /etc/nagios/objects/.

```
define command {
    command_name    check_mongodb
    command_line    $USER1$/check_mongodb.sh -h $ARG1$ -u $ARG2$ -x $ARG3$
}
```

Below is an example of the check\_command in a Nagios service to check that the server specified in ~nagios/.credentials/credentials-filename is the primary server.

```
check_command check_mongodb mongodb1.qa.example.com monitor primary
```

## Adding a Custom Command

It is common to want to monitor things in data that are very application-specific.  For instance, the number of credit card charges that failed within the last N minutes.  Below is some example code showing how to do this for a count query (so no jq filter is needed).  Unlike the existing checks, which are all booleans, the warning and critical values are applicable here.  To fully implement this, all that is needed is a command name, a database query, and default values for warning and critical levels.

```bash
custom )
    dbQuery='db.collectionName.find({
        MongoDB query here
    }).count()'
    if ! [[ -v warning ]]
    then
        warning=default-integer-value
    fi
    if ! [[ -v critical ]]
    then
        critical=default-integer-value
    fi
    output=$(mongoExec "$dbQuery")
    count=$((10#$output))
    if (($? != $EXIT_OK))
    then
        echo "CRITICAL - Invalid count = $output"
        exit $EXIT_CRITICAL
    fi
    if (($count >= $critical))
    then
        echo "CRITICAL - $output"
        exit $EXIT_CRITICAL
    fi
    if (($count >= $warning))
    then
        echo "WARNING - $output"
        exit $EXIT_WARNING
    fi
    message="OK - $output"
    status=$EXIT_OK
    ;;
```
