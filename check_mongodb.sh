#!/bin/bash
# Copyright (c) 2017-2018 Lyle Frost <https://www.cnz.com>.
#
 
# The standard Nagios plugin exit statuses
declare -ir EXIT_OK=0
declare -ir EXIT_WARNING=1
declare -ir EXIT_CRITICAL=2

# Set shell options.
if ! set -fu -o pipefail
then
    echo 'CRITICAL - error setting shell options.'
    exit $EXIT_CRITICAL
fi
if ! shopt -s extglob
then
    echo 'CRITICAL - error enabling shell option extglob.'
    exit $EXIT_CRITICAL
fi

# External commands
declare -r JQ='/usr/bin/jq'
if ! [ -x "$JQ" ]
then
    echo "CRITICAL - jq command $JQ not found."
    exit $EXIT_CRITICAL
fi
declare -r MONGO='/usr/bin/mongo'
if ! [ -x "$MONGO" ]
then
    echo "CRITICAL - mongo command $MONGO not found."
    exit $EXIT_CRITICAL
fi
declare -r STAT='/usr/bin/stat'
if ! [ -x "$STAT" ]
then
    echo "CRITICAL - stat command $STAT not found."
    exit $EXIT_CRITICAL
fi

# Constants
declare -r DEFAULT_DB_AUTH='admin'
declare -r MONGOPASS_FILE=~/.mongopass
if ! [ -e "$MONGOPASS_FILE" ]
then
    echo "CRITICAL - Password file $MONGOPASS_FILE does not exist."
    exit $EXIT_CRITICAL
fi
if [[ "$("$STAT" --printf='%a' "$MONGOPASS_FILE")" != *00 ]]
then
    echo "CRITICAL - Password file $MONGOPASS_FILE must not be group or world accessible."
    exit $EXIT_CRITICAL
fi

# Variables
declare mongoHostname=''
declare mongoUsername=''
declare cmd=''
declare message='CRITICAL - no command was run'
declare -i status=$EXIT_CRITICAL

# Trim all leading and trailing whitespace from a string.
# Usage: trim s
function trim
{
    if (($# != 1))
    then
        echo 'CRITICAL - Invalid arguments to trim.'
        exit $EXIT_CRITICAL
    fi
    local s="$1"

    # Remove leading whitespace.
    s=${s##+([[:space:]])}

    # Remove trailing whitespace.
    s=${s%%+([[:space:]])}

    echo "$s"
}

# Encode a string per http://www.faqs.org/rfcs/rfc3986.html
# Usage: rawurlencode s
function rawurlencode
{
    if (($# != 1))
    then
        echo 'CRITICAL - Invalid arguments to rawurlencode.'
        exit $EXIT_CRITICAL
    fi
    local s="$1"
    local encoded=''  # input string after encoding
    local i           # loop index
    local n=${#s}     # length of the input string
    local ic          # input char
    local oc          # output char

    for ((i = 0; i < n; i++))
    do
        ic="${s:$i:1}"
        case "$ic" in
            # From secion 2.3 of RFC 3986:  unreserved  = ALPHA / DIGIT / "-" / "." / "_" / "~"
            [a-zA-Z0-9-._~] )
                oc="$ic"
                ;;
            * )
                printf -v oc '%%%02x' "'$ic"
        esac
        encoded+="$oc"
    done

    echo "$encoded"
}

# Read database credentials from a file in the same format as used by
# Postgres (hostname:port:database:username:password) and find first
# matching hostname / username.
# Usage: mongoCredentials hostname username
function mongoCredentials
{
    if (($# != 2))
    then
        echo 'CRITICAL - Invalid arguments to mongoCredentials.'
        exit $EXIT_CRITICAL
    fi
    local hostname="$1"
    local username="$2"
    local line=''
    local lineHostname=''
    local linePort=''
    local lineDatabase=''
    local lineUsername=''
    local linePassword=''

    while read line
    do
        line=$(trim "$line")
        if [ ${line:0:1} == '#' ]
        then
            # Skip comment line.
            continue
        fi
        lineHostname=${line%%:*}
        line=${line#*:}
        linePort=${line%%:*}
        line=${line#*:}
        lineDatabase=${line%%:*}
        line=${line#*:}
        lineUsername=${line%%:*}
        linePassword=${line#*:}
        if [ "$lineHostname" == "$hostname" ] && [ "$lineUsername" == "$username" ]
        then
            mongoHostname="$hostname"
            mongoPort="$linePort"
            mongoDatabase="$lineDatabase"
            mongoUsername="$lineUsername"
            mongoPassword="$linePassword"
            return $EXIT_OK
        fi
    done < "$MONGOPASS_FILE"

    return $EXIT_CRITICAL
}

# Execute a MongoDB command.
# Usage: monogExec dbQuery [jqFilter]
# If no jq filter specified, full output of the MongoDB query is returned.
function mongoExec
{
    local dbQuery="$1"
    if (($# > 1))
    then
        local jqFilter="$2"
    fi
    if (($# > 2))
    then
        echo 'CRITICAL - Invalid arguments to mongoExec.'
        exit $EXIT_CRITICAL
    fi

    encodedPassword=$(rawurlencode "$mongoPassword")
    if ! mongoOutput=$("$MONGO" --quiet "mongodb://$mongoUsername:$encodedPassword@$mongoHostname/$mongoDatabase?authSource=$DEFAULT_DB_AUTH" --eval "JSON.stringify($dbQuery)" 2>&1)
    then
        echo "CRITICAL - Command failed: $mongoOutput."
        exit $EXIT_CRITICAL
    fi

    if [[ -v jqFilter ]]
    then
        if ! jqOutput=$(echo "$mongoOutput" | "$JQ" "$jqFilter" 2>&1)
        then
            echo "CRITICAL - Parsing output of command failed: $jqOutput."
            exit $EXIT_CRITICAL
        fi
    else
        jqOutput="$mongoOutput"
    fi

    echo "$jqOutput"
    return $EXIT_OK
}

# Process the command line.
while getopts ':h:u:x:w:c:' opt
do
    case "$opt" in
    h )
        # MongoDB host name
        hostname="$OPTARG"
        ;;
    u )
        # MongoDB username
        username="$OPTARG"
        ;;
    x )
        cmd="$OPTARG"
        ;;
    w )
        if ! warning=$((10#$OPTARG))
        then
            echo 'CRITICAL - w value must be a decimal integer.'
            exit $EXIT_CRITICAL
        fi
        ;;
    c )
        if ! critical=$((10#$OPTARG))
        then
            echo 'CRITICAL - c value must be a decimal integer.'
            exit $EXIT_CRITICAL
        fi
        ;;
    \? )
        echo 'CRTICIAL - invalid command line option'
        exit $EXIT_CRITICAL
        ;;
    esac
done
shift $(($OPTIND - 1))
if (($# != 0))
then
    echo 'CRITICAL - Invalid command line option or argument.'
    exit $EXIT_CRITICAL
fi
if [ -z "$hostname" ]
then
    echo 'CRITICAL - h option is required.'
    exit $EXIT_CRITICAL
fi
if [ -z "$username" ]
then
    echo 'CRITICAL - u option is required.'
    exit $EXIT_CRITICAL
fi
if [ -z "$cmd" ]
then
    echo 'CRITICAL - x option is required.'
    exit $EXIT_CRITICAL
fi

# Load the MongoDB credentials for the specified host and user.
if ! mongoCredentials "$hostname" "$username"
then
    echo "CRITICAL - Credentials for host $hostname and username $username not found."
    exit $EXIT_CRITICAL
fi

case "$cmd" in
primary )
    output=$(mongoExec 'db.isMaster()' '.ismaster')
    if [ "$output" != 'true' ]
    then
        echo "CRITICAL - ismaster = $output"
        exit $EXIT_CRITICAL
    fi
    message="OK - ismaster = $output"
    status=$EXIT_OK
    ;;
secondary )
    output=$(mongoExec 'db.isMaster()' '.ismaster')
    if [ "$output" != 'false' ]
    then
        echo "CRITICAL - ismaster = $output"
        exit $EXIT_CRITICAL
    fi
    message="OK - ismaster = $output"
    status=$EXIT_OK
    ;;
replication )
    output=$(mongoExec 'rs.status()' '.ok')
    if [ "$output" != '1' ]
    then
        echo "CRITICAL - $output"
        exit $EXIT_CRITICAL
    fi
    message="OK - rs.status.ok = $output"
    status=$EXIT_OK
    ;;
\? )
    message="CRITICAL - invalid command $cmd"
    status=$EXIT_CRITICAL
    ;;
esac

echo "$message"
exit $status
